﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {


    public GameObject _enemy;
    public Transform torreta;
    public Transform gunEnd;

    public Transform shot;
    public GameObject bulletPrefab;
    public float distance;
    

    void Start()
    {
        _enemy = GetComponent<GameObject>();
    }

    
    void Update ()
    {
        GameObject _enemy = GameObject.FindWithTag("Enemy");
        distance = Vector3.Distance(transform.position, _enemy.transform.position);
        
        Debug.Log(distance);
        if (distance <= 6.0f)
        {
            torreta.transform.LookAt(_enemy.transform.position);
            gunEnd.transform.LookAt(_enemy.transform.position);
            shot.position = gunEnd.transform.position;
            StartCoroutine(ShootEnemy());
        } 
        
    }
	
   
    IEnumerator ShootEnemy()
    {
        while (distance <= 6.0f)
        {
            GameObject bullet = Instantiate(bulletPrefab, shot.position, Quaternion.identity);
            yield return new WaitForSeconds(2.0f);
            Destroy(bullet, 3.0f);
            
        } yield return null;

    }
    
    
}
