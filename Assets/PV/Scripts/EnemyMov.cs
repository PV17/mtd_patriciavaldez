﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnemyMov : MonoBehaviour {

    private NavMeshAgent agent;
    private GameObject target;

    [SerializeField]
    private bool _reachedTarget;
    [SerializeField]
    private GameObject _explosionPrefab;

    [SerializeField]
    private bool _dead;
    public int healthEnemy = 3;

    [SerializeField]
    private GameObject bulletPrefab;
    private Vector3 posTarget;
    public float distanceTarget;


    [SerializeField]
    private HeadQuaters headquaters;
    


    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();

        target = GameObject.Find("TargetEnemies");
        agent.SetDestination(target.transform.position);
        
	}
	
	// Update is called once per frame
	void Update () {
        posTarget = GameObject.Find("headQuaters").transform.position;
        distanceTarget = Vector3.Distance(transform.position, posTarget);
        
        if(distanceTarget < 6f && !_reachedTarget)
        {
            _reachedTarget = true;
            StartCoroutine(ReachedTarget());

        }

        Dead();
	}

    public IEnumerator ReachedTarget()
    {
        GameObject hq = GameObject.Find("headQuaters");
        while (true)
        {
            yield return new WaitForSeconds(1);
            Instantiate(_explosionPrefab, new Vector3(hq.transform.position.x - 1.5f, 3.14f, -21.14f), Quaternion.identity);
            Demage();
            headquaters.HealthHQ--;
            Destroy(gameObject, 2.0f);
            

            if (headquaters.HealthHQ <= 0 )
            {
                yield return new WaitForSeconds(3);
                SceneManager.LoadScene("Loose");
                yield break;
            }
        }
    }

   
    public void Demage()
    {
        healthEnemy -= 1;
        if(healthEnemy <= 0 && !_dead)
        {
            _dead = true;
            Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    void Dead()
    {
        if (healthEnemy <= 0 )
        {
            Destroy(gameObject);
        }
    }


    void OnTriggerEnter(Collider col)
    {
        if(col.name == "Bullet" && healthEnemy >= 1)
        {
            healthEnemy -= 1;
        }
    }
}
