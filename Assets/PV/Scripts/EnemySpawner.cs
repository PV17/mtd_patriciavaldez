﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

   
    public enum EnemyTypes
    {
        buggy,
        helicopter,
        hover
    };

    void Start()
    {
        
        StartCoroutine(SendWave());
        
    }

    [System.Serializable] public struct Waves
    {
        public EnemyTypes[] wave;
    }

    public Waves[] waves;

    [SerializeField] private GameObject buggyPrefab;
    [SerializeField] private GameObject helicopterPrefab;
    [SerializeField] private GameObject hoverPrefab;
    [SerializeField] private int currentWave = 0;
    [SerializeField] private int currentEnemy = 0;
    [SerializeField] private int timeBetweenWaves = 5;
    [SerializeField] private int timeBetweenEnemies = 3;
    [SerializeField] private GameObject target;
    
    public IEnumerator SendWave()
    {
        //Dentro una serie de loops que estan enviando enemigos cada cierto tiempo 
        //  y oleadas  cada cierto tiempo

        while (currentEnemy < waves[currentWave].wave.Length)
        {
            switch (waves[currentWave].wave[currentEnemy])
            {
                case EnemyTypes.buggy:
                    Instantiate(buggyPrefab, transform.position, Quaternion.identity);
                    break;
                case EnemyTypes.helicopter:
                    Instantiate(helicopterPrefab, transform.position, Quaternion.identity);
                    break;
                case EnemyTypes.hover:
                    Instantiate(hoverPrefab, transform.position, Quaternion.identity);
                    break;
            }

            yield return new WaitForSeconds(timeBetweenEnemies);
            currentEnemy++;
            
        }
        currentWave++;
        if(currentWave < waves.Length)
        {
            currentEnemy = 0;
            yield return new WaitForSeconds(timeBetweenWaves);
            nextWave();
        }
        else { yield return null; }
    } 

    void nextWave()
    {
        StartCoroutine(SendWave());
    }


   
}
