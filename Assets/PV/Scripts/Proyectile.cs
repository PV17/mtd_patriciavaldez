﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectile : MonoBehaviour {
    
    Rigidbody rb;
    public float _bulletSpeed = 100.0f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(0, 0, _bulletSpeed, ForceMode.Impulse);
        Destroy(gameObject, 5f);
    }

  
}
