﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace raycast
{
    public class Player : MonoBehaviour
    {

        [SerializeField] private GameObject gunPrefab;
        [SerializeField] private GameObject missilePrefab;
        [SerializeField] private GameObject sniperWeapon;
        [SerializeField] private GameObject holdingWeapon;
        public int countBonus;
        public int count;
        

        void Start()
        {
            countBonus = 4;
            count = countBonus;
        }


        public void CreateWeapon(string weaponName)
        {
            
            if (holdingWeapon != null && count <= 3)
                return;
            switch (weaponName)
            {
                case "gunPrefab":
                    holdingWeapon = Instantiate(gunPrefab);
                    Debug.Log("Hola");
                    break;
                case "missilePrefab":
                    holdingWeapon = Instantiate(missilePrefab);
                    break;
                case "sniperWeapon":
                    holdingWeapon = Instantiate(sniperWeapon);
                    break;
            }
        }

        // Update is called once per frame
        void Update()
        {
            
            if (holdingWeapon != null )
            {
                countBonus -= 4;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    holdingWeapon.transform.position = hitInfo.point;
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (hitInfo.transform.CompareTag("Placement"))
                        {
                            holdingWeapon = null;
                            Destroy(hitInfo.collider);
                        }
                    }
                }
            } 
        }
    }
}

