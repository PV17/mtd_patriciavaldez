﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMovement : MonoBehaviour {

    [SerializeField] private Vector2 mousePos;
    [SerializeField] private float screenWidth;
    [SerializeField] private float screenHeight;
    [SerializeField] private float camMoveSpeed = 5;
    

    void Update()
    {
        MoveCamera();
    } 

    void MoveCamera()
    {
        mousePos = Input.mousePosition;
        screenWidth = Screen.width;
        screenHeight = Screen.height;

        Vector3 cameraPos = transform.position;

        if (mousePos.x > (screenWidth - 10) && cameraPos.x < 1)
        {
            cameraPos.x += Time.deltaTime * camMoveSpeed;
        }
        if(mousePos.x < 10 && cameraPos.x > -10)
        {
            cameraPos.x -= Time.deltaTime * camMoveSpeed;
        }

        if (mousePos.y > (screenHeight -20) && cameraPos.z < -20f)
        {
            cameraPos.z += Time.deltaTime * camMoveSpeed;
        }

        if (mousePos.y < 20 && cameraPos.z > -30f)
        {
            cameraPos.z -= Time.deltaTime * camMoveSpeed;
        }
        transform.position = cameraPos;

    }

}
